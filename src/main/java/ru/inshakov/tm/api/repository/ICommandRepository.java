package ru.inshakov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByName(@NotNull String name);

    AbstractCommand getCommandByArg(@NotNull String arg);

    @NotNull
    Collection<AbstractCommand> getCommands();

    @NotNull
    Collection<AbstractCommand> getArguments();

    void add(@NotNull AbstractCommand command);

}
