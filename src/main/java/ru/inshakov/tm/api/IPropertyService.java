package ru.inshakov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.inshakov.tm.api.other.ISaltSetting;

public interface IPropertyService extends ISaltSetting {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthor();

    @NotNull
    String getAuthorEmail();

    String getFileBase64Path();

    String getFileBinaryPath();

}
