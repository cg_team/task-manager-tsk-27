package ru.inshakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IRepository;
import ru.inshakov.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(@NotNull E entity) {
        entities.add(entity);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public void addAll(@NotNull List<E> entity) {
        entities.addAll(entity);
    }

    @Nullable
    @Override
    public List<E> findAll() {
        return entities;
    }

    @Override
    public @Nullable E findById(final @NotNull String id) {
        return entities.stream().filter(item -> id.equals(item.getId())).findFirst().orElse(null);
    }

    @Override
    public void remove(final @NotNull E entity) {
        entities.remove(entity);
    }

    @Override
    public @Nullable E removeById(@NotNull String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        entities.remove(entity);
        return entity;
    }

}
