package ru.inshakov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.inshakov.tm.api.IPropertyService;
import ru.inshakov.tm.dto.Domain;
import ru.inshakov.tm.enumerated.Role;
import ru.inshakov.tm.service.PropertyService;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;

public class DataBase64SaveCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "data-save-base64";
    }

    @Nullable
    @Override
    public String description() {
        return "save data to base64 file";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @Nullable final String fileBase64Path = propertyService.getFileBase64Path();
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(fileBase64Path);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        @NotNull final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = new BASE64Encoder().encode(bytes);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(fileBase64Path);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
